import {
  PERIODIC_TABLE_DOUBLE_LOWER,
  PERIODIC_TABLE_SINGLE_LOWER
} from './constants';
import templateHTML from './template.html';

const template = document.createElement('template');
template.innerHTML = templateHTML;

class PeriodicElement extends HTMLElement {
  content: string | null = '';
  start: string | null = '';
  element: string | null = '';
  end: string | null = '';

  connectedCallback() {
    this.content = this.getAttribute('content');
    this.start = this.content || this.getAttribute('start');
    this.element = this.getAttribute('element');
    this.end = this.getAttribute('end');

    this.appendChild(document.importNode(template.content, true));
    this.generateChunks();
    this.renderChunks();
  }

  generateChunks() {
    if (!this.content) {
      // Use provided chunks

      return;
    }

    const content = this.content;
    const contentLower = content.toLowerCase();
    const splitContent = (text: string) => {
      const [start, end] = contentLower.split(text);
      this.start = content.slice(
        contentLower.indexOf(start),
        contentLower.indexOf(start) + contentLower.indexOf(text)
      );
      this.element = text;
      this.end = content.slice(contentLower.indexOf(end));
    };

    if (this.element) {
      // Provided element

      splitContent(this.element);
    } else {
      // Double elements
      [...(contentLower.match(/..?/g) || [])].some((text: string) => {
        if (PERIODIC_TABLE_DOUBLE_LOWER.includes(text)) {
          splitContent(text);
          return true;
        }
        return;
      });

      if (!this.element) {
        // Fallback to Single elements
        contentLower.split('').some((text: string) => {
          if (PERIODIC_TABLE_SINGLE_LOWER.includes(text)) {
            splitContent(text);
            return true;
          }
          return;
        });
      }
    }
  }

  renderChunks() {
    const el = this.querySelector('.periodic-content') as HTMLElement;
    if (!el) {
      return;
    }

    const chunks: {
      text: string | null;
      class: string;
    }[] = [
      {
        text: this.start,
        class: 'pe-item'
      },
      {
        text: this.element,
        class: 'pe-item pe-item-selected'
      },
      {
        text: this.end,
        class: 'pe-item'
      }
    ];

    for (const item of chunks) {
      const span: HTMLElement = document.createElement('span');
      span.innerHTML = item.text || '';
      span.classList.add(...item.class.split(' '));
      el.appendChild(span);
    }
  }
}

customElements.define('periodic-element', PeriodicElement);
